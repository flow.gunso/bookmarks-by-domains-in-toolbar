#!/bin/bash

# See https://gitlab.com/flow.gunso/bookmarks-by-domains-in-toolbar/-/issues/9
#branch="$(git branch --show-current)"
#if ! [[ "$branch" =~ v[0-9]+\.[0-9]+ ]]; then
#    echo "Branch name does not match a release branch"
#    exit 1
#fi

web_ext=node_modules/web-ext/bin/web-ext
$web_ext build \
    -s src/ \
    -c package.json \
    --overwrite-dest
