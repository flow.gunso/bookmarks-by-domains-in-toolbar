/**
 * Copyright (C) 2021 flow.gunso@gmail.com
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * Sorter. The core.
 */

import { onReject } from "/js/utilities.js";


/**
 * Return the domain name of a bookmark that target an http/https URL.
 * @param {bookmarks.BookmarkTreeNode} bookmark The bookmark to return the domain from.
 */
function getDomainFromBookmark(bookmark) {
    try {
        var url = new URL(bookmark.url);
        if (url.protocol !== "https:" && url.protocol !== "http:") { return null; }
        return url.hostname.split('.').splice(-2).join('.');
    } catch (e) {
        if (e instanceof TypeError) { console.log("Bookmark " + bookmark.id +  " does not refer to a valid URL"); }
        else { console.error(`Caught ${e}!`); }
        return null;
    }
}

/**
 * Returns a bookmark folder named with the given title. Is a thenable function of browser.bookmarks.search promise.
 * @param {<Array>bookmarks.BookmarkTreeNode} nodes The results of a browser.bookmarks.search on title.
 * @param {String} domain The domain name to use as the bookmark folder title.
 */
async function getOrCreateBookmarkFolder(nodes, domain){
    var node;
    if (nodes.length == 0){
        var bookmarkDetails = {title: domain, type: "folder", parentId: "toolbar_____"};
        node = browser.bookmarks.create(bookmarkDetails)
            .then((node) => {return node})
            .catch(onReject);
    } else if (nodes.length == 1){
        node = nodes[0];
    } else {
        console.log("Found several bookmarks folders titled " + domain + ", returning the first one.");
        node = nodes[0];
    }
    console.log(node);
    return node;
}

/**
 * Sort a given bookmark to a bookmark folder using the domain name it targets to.
 * @param {bookmarks.BookmarkTreeNode} bookmark The bookmark to sort.
 */
export async function sortBookmark(bookmark) {
    var domain = getDomainFromBookmark(bookmark);
    console.log(domain);
    if (domain === null) { return null; }

    var folder = await browser.bookmarks.search({title: domain })
        .then((results) => getOrCreateBookmarkFolder(results, domain))
        .catch(onReject);
    console.log(folder);
    
    var destination = {parentId: folder.id};
    await browser.bookmarks.move(bookmark.id, destination)
        .then(() => {console.log(`Moved ${bookmark.id} to ${folder.id}.`)})
        .catch(onReject);
}

/**
 * Import, sort bookmarks from the given bookmark folder.
 * @param {bookmarks.BookmarkTreeNode} folder The bookmark folder to start the import from.
 * @param {Number} depth Indicate the recursion level.
 */
export async function importBookmarks(folder, depth = 0) {
    if (depth === 0) { folder = folder[0]; }

    for (var child of folder.children) {
        console.log(`${child.id}, ${child.type}, ${child.title}`);
        if (child.type === "folder") {
            await importBookmarks(child, depth + 1);
            await browser.bookmarks.remove(child.id)
                .then(() => { console.log(`Deleted bookmark folder ${child.id}`)})
                .catch(onReject);
        } else if (child.type === "bookmark") {
            await sortBookmark(child);
        } else if (child.type === "separator") {
            await browser.bookmarks.remove(child.id)
                .then(() => { console.log(`Deleted bookmark separator ${child.id}`)})
                .catch(onReject);
        }
    }
}