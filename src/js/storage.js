/**
 * Copyright (C) 2021 flow.gunso@gmail.com
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * Storage wrappers adapted from https://github.com/bitwarden/browser/blob/master/src/services/browserStorage.service.ts
 */


var StorageService = function() {
    this.storageApi = chrome.storage.local;
}

StorageService.prototype = {

    get: async function(key) {
        return new Promise((resolve) => {
            this.storageApi.get(key, (obj) => {
                if (obj != null && obj[key] != null) {
                    resolve(obj);
                }
                resolve(null);
            });
        });
    },

    set: async function(key) {
        return new Promise((resolve) => {
            this.storageApi.set(key, () => {
                resolve();
            });
        });
    },

    remove: async function(key) {
        return new Promise((resolve) => {
            this.storageApi.remove(key, () => {
                resolve();
            });
        });
    },

    clear: async function() {
        return new Promise((resolve) => {
            this.storageApi.clear(() => {
                resolve();
            });
        });
    }
}

export var storageService = new StorageService();
