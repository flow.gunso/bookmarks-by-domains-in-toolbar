# Bookmarks by domains in the toolbar
[<img src="https://blog.mozilla.org/addons/files/2020/04/get-the-addon-fx-apr-2020.svg" width="80"/>](https://addons.mozilla.org/en-US/firefox/addon/bookmarks-domains-toolbar/?utm_source=gitlab)

A Firefox WebExtension that sort bookmarks into domain named folders in the bookmark toolbar.


## Features
* Import then sort existing bookmarks into the toolbar
* Sort bookmarks on creation


## Roadmap
The planned changes to reach v1 are [listed in the v1 milestone](https://gitlab.com/flow.gunso/bookmarks-by-domains-in-toolbar/-/milestones/1).

## License

GPL-3.0-only