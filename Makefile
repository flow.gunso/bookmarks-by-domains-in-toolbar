NODE_MODULES?=node_modules
BASH?=bash
NPM?=npm
WEB_EXT?=${NODE_MODULES}/web-ext/bin/web-ext


env:
	${NPM} install

lint:
	${WEB_EXT} lint -s src/ --no-config-discovery

publish:
	bash .ci/publish.sh

build:
	bash .ci/build.sh
