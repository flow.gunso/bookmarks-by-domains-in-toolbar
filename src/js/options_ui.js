/** 
 * Copyright (C) 2021 flow.gunso@gmail.com
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * Option page related.
 */

import { storageService } from "/js/storage.js";
import { onReject } from "/js/utilities.js";


/* Bookmark folder list */

/**
 * Build an UTF-8 directory tree branch.
 * @param {Number} depth The branch depth.
 * @param {String} name The branch name.
 */
function buildBookmarkFolderBranch(depth, name) {
    var line = "";
    if (depth > 0) { line += "\u251c"; } 
    if (depth > 1) { line += "\u253c".repeat(depth - 1); }
    line += "\u00a0";
    line += name;
    return line;
}

/**
 * Display a directory tree from a BookmarkTreeNode using recursion.
 * @param {bookmarks.BookmarkTreeNode} node The current BookmarkTreeNode.
 * @param {Number} depth The recursion depth.
 */
function addBookmarksFoldersToImportFromList(node, depth = 0) {
    if (depth === 0) { node = node[0]; }

    var select = document.getElementById("import_from");
    for (var child of node.children) {
        if (child.type === "folder" && child.id !== "toolbar_____") {
            var option = document.createElement("option");
            option.value = child.id;
            option.id =  child.id;
            option.textContent = buildBookmarkFolderBranch(depth, child.title);
            select.appendChild(option);
            addBookmarksFoldersToImportFromList(child, depth + 1);
        }
    }
    return;
}

/**
 * Load the whole bookmark tree into the select's option and preselect a value.
 */
async function loadBookmarkTreeOptions() {
    await browser.bookmarks.getTree().then(addBookmarksFoldersToImportFromList);

    var obj = await storageService.get("import_from");
    if (obj === null) {
        var option = document.getElementById("import_from_default");
    } else {
        import_from = obj.import_from;
        var option = document.getElementById(import_from);
    }

    var button = document.getElementById('action_import');
    button.disabled = false;
}
document.addEventListener('DOMContentLoaded', loadBookmarkTreeOptions);


/* Reset preferences */

/**
 * Safely validate the storage reset, by displaying a countdown.
 */
async function validateReset() {
    var validate_reset = document.getElementById('validate_reset');
    var countdown_reset = document.getElementById('countdown_reset');
    var reset = document.getElementById('action_reset');
    var ask_reset = document.getElementById('action_ask_reset');

    validate_reset.hidden = false;
    ask_reset.hidden = true;
    
    for ( let i = 3; i > 0; i--) {
        countdown_reset.textContent = i;
        await new Promise(r => setTimeout(r, 1000));
    }

    countdown_reset.hidden = true;
    reset.hidden = false;
}

/**
 * Restore the reset group UI.
 */
function restoreResetGroup(){
    var validate_reset = document.getElementById('validate_reset');
    var countdown_reset = document.getElementById('countdown_reset');
    var reset = document.getElementById('action_reset');
    var ask_reset = document.getElementById('action_ask_reset');

    validate_reset.hidden = true;
    countdown_reset.hidden = false;
    reset.hidden = true;
    ask_reset.hidden = false;

    var option = document.getElementById("import_from_default");
    option.selected = true;
}

/**
 * Clear the extension storage.
 */
async function doReset(){
    await storageService.clear();
    restoreResetGroup();
}
document.getElementById('action_ask_reset').addEventListener('click', validateReset);
document.getElementById('action_cancel_reset').addEventListener('click', restoreResetGroup);
document.getElementById('action_reset').addEventListener('click', doReset);


/* Import from setter */

/**
 * Set the import from on the select's option change.
 * @param {Event} event The event from the Element event listener.
 */
async function setOptionImportFrom(event) {
    var import_from = event.target.value;
    await storageService.set({import_from});
}
document.getElementById('import_from').addEventListener('change', setOptionImportFrom);


/* Importer */

/**
 * Change an HTML element hidden, style's color and textContent attributes.
 * @param {Element} element The HTML element element to change.
 * @param {Boolean} hidden It's hidden attribute.
 * @param {String} color It's style's color attribute.
 * @param {String} textContent It's textContent attribute.
 */
function setStatusOfElement(element, hidden = true, color = null, textContent = null) {
    if (hidden === true) { element.hidden = hidden };
    element.style.color = color;
    element.textContent = textContent;
    if (hidden === false) { element.hidden = hidden };
}

/**
 * Wrapper around setStatusOfElement exclusively for the HTML element with the id status_import .
 * @param {Boolean} hidden It's hidden attribute.
 * @param {String} color It's style's color attribute.
 * @param {String} textContent It's textContent attribute.
 */
function setStatusImport(hidden = true, color = null, textContent = null) {
    setStatusOfElement(document.getElementById('status_import'), hidden, color, textContent);
}

/**
 * Handle the response sent from the sendMessageImnport callback. Allow to modify the UI to inform user.
 * @param {Object} response 
 */
function handleResponse(response) {
    if (response instanceof Promise) {
        // lock the import button
        // await the promise
        console.log("Has promise");
    } else if (response instanceof Object) {
        var status_element = document.getElementById('status_import');
        if (response.hasOwnProperty("error")) {
            setStatusImport(false, "red", response.error);
        }
    }
}

/**
 * Send a message over runtime with an import action.
 */
function sendMessageImport() {
    setStatusImport();

    var send = browser.runtime.sendMessage({action: "import"});
    send.then(handleResponse, onReject);
}
document.getElementById('action_import').addEventListener('click', sendMessageImport);