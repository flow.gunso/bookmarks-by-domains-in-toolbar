/**
 * Copyright (C) 2021 flow.gunso@gmail.com
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * Background events listeners and handlers.
 */

import { storageService } from "/js/storage.js";
import { sortBookmark, importBookmarks } from "/js/sorter.js";
import { onReject } from "/js/utilities.js";


/**
 * Handle the import, making sure the import_from is set prior to starting the import.
 */
export async function handleImport() {

    var obj = await storageService.get("import_from");
    if (obj === null) {
        return {error: "Import bookmarks from is not defined!"};
    } else {
        var import_from = obj.import_from;
    }
    await browser.bookmarks.getSubTree(import_from)
      .then(importBookmarks)
      .catch(onReject);
}

/**
 * Handle the browser.bookmarks.onCreated event calling sorter.js's sortBookmark.
 * @param {String} id The new bookmark item's ID
 * @param {bookmarks.BookmarkTreeNode} bookmark Information about the new bookmark item.
 */
export async function handleBookmarkOnCreated(id, bookmark) {
    await sortBookmark(bookmark);
}

/**
 * Handle message received from the browser.runtime.onMesssage events. Are handled only message with an action, that action being `import`.
 * Follow the MDN's documentation.
 * @param {object} message The message itself. This is a JSON-ifiable object.
 * @param {runtime.MessageSender} sender A runtime.MessageSender object representing the sender of the message.
 * @param {object} sendResponse A function to call, at most once, to send a response to the message. 
 */
export function handleMessage(message, sender, sendResponse) {
    var response = undefined;
    if (message.hasOwnProperty("action")) {
      switch(message.action) {
        case "import":
          response = handleImport();
      }
    }
    sendResponse(response);
}

browser.runtime.onMessage.addListener(handleMessage);
browser.bookmarks.onCreated.addListener(handleBookmarkOnCreated);